---
title: "Übung"
date: 2021-12-05T11:00:46+02:00
weight: 220
---

Basis für die Übung ist die OAI-Schnittstelle von réro doc:

* Base URL: [http://doc.rero.ch/oai2d](http://doc.rero.ch/oai2d)
* Identify: [http://doc.rero.ch/oai2d?verb=Identify](http://doc.rero.ch/oai2d?verb=Identify)
* [Dokumentation](https://doc.rero.ch/help/harvest)


Nutzen Sie die Schnittstelle, um die folgenden Fragen zu beantworten. 

* Welche Metadatenformate bietet das Repository an?
* Wie sieht die Struktur der Identifier in diesem Repository aus?
* Suchen Sie in [réro doc](https://doc.rero.ch/?ln=de) mit dem Stichwort "metadata". Kopieren Sie den Identifier der ersten Aufnahme auf der Trefferliste und holen Sie damit diese Aufnahme über die OAI-Schnittstelle im MARC-Format ab.
* Wann wurde diese Aufnahme zuletzt bearbeitet?
* Welche Sets bietet das Repository an?
* In welchen Sets ist die vorher abgefragte Aufnahme enthalten?
* Welcher Titel wird für Dissertationen, die im letzten Halbjahr bearbeitet wurden, zuerst aufgeführt? Was ist das Erscheinungsdatum dieser Publikation?
* Suchen Sie nach den Records, die seit Januar vor zwei Jahren bearbeitet wurden. Holen Sie auch die nächste Seite ab.

### Weitere Beispiele
Falls Sie Zeit dafür haben oder die Arbeit mit OAI vertiefen wollen, können Sie die Fragen auch für eine Schnittstelle Ihrer Wahl beantworten.

Mögliche OAI-Schnittstellen:
* [Nationalbibliothek](https://helveticat.nb.admin.ch/view/oai/41SNL_51_INST/request)
* [ZORA](https://www.zora.uzh.ch/cgi/oai2)
* [SERVAL](http://serval.unil.ch/oaiprovider)
* [e-periodica](https://www.e-periodica.ch/oai/dataprovider)
* [DNB](https://services.dnb.de/oai/repository)

### Lösungen
<br/>
<details>
  <summary>Welche Metadatenformate bietet das Repository an?</summary>
  <ul>
    <li>Mit einer Abfrage mit dem Request ListMetadataFormats kann diese Frage beantwortet werden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Wie sieht die Struktur der Identifier in diesem Repository aus?</summary>
  <ul>
    <li>Meistens ist die Struktur der Identifier in den Metainformationen zum Repository, Request Identify, dokumentiert.</li>
    <li>Wenn die Struktur der Identifier nicht dokumentiert ist oder Beispiele nötig sind, hilft es, sich alle Identifiers mit dem Request ListIdentifiers anzuschauen. Daraus lässt sich die korrekte Struktur ableiten.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Suchen Sie auf der Oberfläche des Repositories nach einer Aufnahme mit dem Stichwort "metadata". Kopieren Sie den Identifier der ersten Aufnahme auf der Trefferliste und holen Sie damit diese Aufnahme über die OAI-Schnittstelle ab. Wann wurde diese Aufnahme zuletzt bearbeitet?</summary>
  <ul>
    <li>Dazu muss die Struktur der Identifer bekannt sein, siehe vorhergehende Frage.</li>
    <li>Anschliessend kann die Aufnahme mit dem Request GetRecord abgeholt werden.</li>
    <li>Im Header der Response wird im Tag datestamp das letzte Bearbeitungsdatum ausgegeben.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Welche Sets bietet das Repository an?</summary>
  <ul>
    <li>Sets können mit dem Request ListSets aufgelistet werden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>In welchen Sets ist die vorher abgefragte Aufnahme enthalten?</summary>
  <ul>
    <li>Die Zugehörigkeit eines Records zu einem Set ist im Header im Tag setSpec angegeben.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Welcher Titel wird für Records, die am letzten Montag bearbeitet wurden, zuerst aufgeführt? Was ist das Erscheinungsdatum dieser Publikation?</summary>
  <ul>
    <li>Mit ListRecords und den Argumenten from und until können Records, die in einem bestimmten Zeitraum bearbeitet wurden, abgeholt werden.</li>
    <li>Das Erscheinungsdatum ist in den Metadaten zu finden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Suchen Sie nach den Records, die in diesem Monat bearbeitet wurden. Holen Sie auch die nächste Seite ab.</summary>
  <ul>
    <li>Mit ListRecords und dem Argument from können Records, die seit einem bestimmten Datum bearbeitet wurden, abgeholt werden.</li>
    <li>Die nächste Seite, falls vorhanden, kann mit dem resumptionToken abgeholt werden. Das Token wird am Ende der Response im Tag resumptionToken angegeben</li>
  </ul>
</details>
<br/>