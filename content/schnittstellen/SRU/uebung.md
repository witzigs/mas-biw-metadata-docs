---
title: "Übung"
date: 2021-12-30T109:26:46+02:00
draft: true
---

Beantworten Sie für eine SRU-Schnittstelle Ihrer Wahl folgende Fragen:

* Welche Metadatenformate bietet das Repository an?
* Welche Suchindices stehen für eine Suche im Titel zur Verfügung?
* Suchen Sie nach Einträgen, die vor 1970 erschienen sind. Wie viele Treffer hat diese Suche?
* Holen Sie von dieser Liste die nächste Seite.
* Kombinieren Sie eine Suche nach Titel und Autor.
* Suchen Sie nach einer Phrase im Titel.
* Geben Sie die Daten in einem anderen zur Verfügung stehenden Metadatenformat aus.
* Speichern Sie die erste Seite der ausgegebenen Resultate ab (z.B. mit curl).
* Schauen Sie sich die Dokumentation der CQL an und probieren Sie aus welche Suchoperatoren bei dieser Schnittstelle nutzbar sind.


### Mögliche SRU-Schnittstellen
* [SLSP](https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=explain)
* [K10plus](http://sru.k10plus.de/opac-de-627?version=1.2&operation=explain) und [Dokumentation](https://wiki.k10plus.de/display/K10PLUS/SRU) (verlangt maximumRecords)
* [Jisc](http://discover.libraryhub.jisc.ac.uk:210/discover?operation=explain) und [Dokumentation](https://discover.libraryhub.jisc.ac.uk/support/api/) (verlangt maximumRecords)
* [Library of Congress Online Catalog](http://lx2.loc.gov:210/LCDB?)


### Lösungen
Allgemeiner Hinweis: SRU-Schnittstellen bieten unterschiedliche Möglichkeiten zur Suche an. Es ist daher möglich, dass bestimmte Fragen für bestimmte SRU-Schnittstellen nicht beantwortet werden können.
<br/>
<br/>
<details>
  <summary>Welche Metadatenformate bietet das Repository an?</summary>
  <ul>
    <li>Mit der operation explain können Informationen über die Schnittstelle aufgerufen werden. Darin sind die Metadatenformate im Tag schemaInfo aufgeführt.</li>
    <li>Zum Beispiel: &lt;schemaInfo&gt;&lt;schema identifier="info:srw/schema/1/mods-v3.5" name="mods" sort="true"/&gt;&lt;/schemaInfo&gt;</li>
    <li>Falls über explain keine Informationen gefunden werden, muss externe Dokumentation zur Schnittstelle beigezogen werden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Welche Suchindices stehen für eine Suche im Titel zur Verfügung?</summary>
  <ul>
    <li>Mit der operation explain können Informationen über die Schnittstelle aufgerufen werden. Darin sind die Suchindices im Tag indexInfo aufgeführt.</li>
    <li>Falls über explain keine Informationen gefunden werden, muss externe Dokumentation zur Schnittstelle beigezogen werden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Suchen Sie nach Einträgen, die vor 1970 erschienen sind. Wie viele Treffer hat diese Suche?</summary>
  <ul>
    <li>Dazu muss ein Index für das Publikationsjahr mit einem Operator grösser als durchsucht werden.</li>
    <li>Beispiel: query=dc.date > 1970</li>
  </ul>
</details>
<br/>
<details>
  <summary>Holen Sie von dieser Liste die nächste Seite.</summary>
  <ul>
    <li>Der Abfrage muss ein Parameter startRecord mit der Position des ersten Treffers auf der nächsten Seite mitgegeben werden. Welche Position dies ist, steht im Tag nextRecordPosition.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Kombinieren Sie eine Suche nach Titel und Autor.</summary>
  <ul>
    <li>Dazu muss eine Suche in einem Index für den Titel und eine in einem Index für den Autor mit and verknüpft werden.</li>
    <li>Beispiel: query=dc.title any fish and dc.creator any sanderson</li>
  </ul>
</details>
<br/>
<details>
  <summary>Suchen Sie nach einer Phrase im Titel.</summary>
  <ul>
    <li>Der Suchbegriff muss dazu in doppelten Anführungszeichen eingegeben werden.</li>
    <li>Beispiel: query=dc.title = "lord of the rings"</li>
  </ul>
</details>
<br/>
<details>
  <summary>Geben Sie die Daten in einem anderen zur Verfügung stehenden Metadatenformat aus.</summary>
  <ul>
    <li>Das gewünschte Format wird im Parameter recordSchema angegeben.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Speichern Sie die ausgegebenen Resultate ab (z.B. mit curl).</summary>
  <ul>
    <li>Zum Beispiel: curl 'https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=alma.title=architektur&startRecord=11' > results.xml</li>
  </ul>
</details>
<br/>