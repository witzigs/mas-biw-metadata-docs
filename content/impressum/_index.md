---
title: "Impressum"
date: 2021-12-30T109:26:46+02:00
weight: 40
---
Diese Unterlagen entstanden für den Kurs Bibliothekarische Metadatenformate, Thema Datenaustausch im Rahmen des [MAS Bibliotheks- und Informationswissenschaft](https://www.biw.uzh.ch/de/masbiw.html), Durchführung 2021-2023, der Universität Zürich.

Autorin: Silvia Witzig

## Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>.


## Quellen
Berggren, Anna. Libris as Linked Data. [ELAG](https://www.elag2019.de/), 2019: https://docs.google.com/presentation/d/e/2PACX-1vQF2ek2i3-6spsiMjMcSi03J4Nm4CdRvsupkeQ826w7Ctl4QO4iftttAeGx_dPUpJEhQJ3brCjCVWRZ/pub?start=false&loop=false&delayms=3000#slide=id.g58f40670a4_0_117

Carlson, Scott, Cory Lampert, Darnelle Melvin und Anne Washington. Linked Data for the perplexed librarian. ALA Editions, 2020.

Danowski, Patrick. (Open) Linked Data in Bibliotheken. De Gruyter/Saur, 2013. 

"Data Map der Zentralbibliothek Zürich". ZB Zürich: https://data.zb.uzh.ch/map/books/data-map-der-zentralbibliothek-zurich/page/systemubersicht

Gantert, Klaus. Bibliothekarisches Grundwissen. 9., vollständig neu bearbeitete und erweiterte Auflage. De Gruyter Saur, 2016. 

Geeraert, Friedel. “From the Library Catalogue to the Semantic Web - the Conversion Process from MARC 21 to BIBFRAME 2.0”. 2018: https://doi.org/10.6084/m9.figshare.6731129.v1

Griebel, Rolf, Hildegard Schäffler, und Konstanze Söllner. Praxishandbuch Bibliotheksmanagement. Walter de Gruyter, 2014. 

Lindström, Niklas. National Platform based on BIBFRAME. [BIBFRAME Workshop in Europe](https://www.kb.se/samverkan-och-utveckling/nytt-fran-kb/nyheter-samverkan-och-utveckling/2019-09-25-presentations-from-the-3rd-annual-bibframe-workshop-in-europe.html), 2019: https://www.kb.se/download/18.d0e4d5b16cd18f600eafd/1569324736859/National%20Platform%20Based%20On%20BIBFRAME.pdf

McCallum, Sally H. Developments at the Library of Congress. [BIBFRAME Workshop in Europe](https://www.casalini.it/bfwe2020/), 2020: https://www.casalini.it/bfwe2020/web_content/2020/presentations/mccallum.pdf

McCallum, Sally H. BIBFRAME 100. [BIBFRAME January 2021 Update Forum](https://www.loc.gov/bibframe/news/bibframe-update-mw2021.html), 2021: https://www.loc.gov/bibframe/news/source/bibframe-update-alamw2021-lc.pptx

Morgan, Eric Lease. "An Introduction to the Search/Retrieve URL Service (SRU)". Ariadne Issue 40. 3 Juli 2004: http://www.ariadne.ac.uk/issue40/morgan/

Schreuer, Philip. The Evolution of BIBFRAME: from MARC Surrogate to Web Conformant Data Model. 2018: http://library.ifla.org/2202/1/141-schreur-en.pdf

Suominen, Osma. From MARC silos to Linked Data silos. [SWIB16](https://swib.org/swib16/programme.html), 2016: https://swib.org/swib16/slides/suominen_silos.pdf 

Veltzman, Itai. Ex Libris Linked Data Principles, Vision, and Roadmap. [BIBFRAME January 2021 Update Forum, 2021](https://www.loc.gov/bibframe/news/bibframe-update-mw2021.html): https://www.loc.gov/bibframe/news/pdf/bibframe-exlibris-alamw2021.pdf

"Vocabularies". W3C: https://www.w3.org/standards/semanticweb/ontology

Wallis, Richard. The three linked data choices for libraries. Data Liberate, 2018: https://www.dataliberate.com/2018/05/22/the-three-linked-data-choices-for-libraries/

Zeng, Marcia Lei, und Jian Qin. Metadata. Second edition. Facet Publishing, 2016. 