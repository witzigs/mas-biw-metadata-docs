---
title: "Bibliothekarische Metadatenformate"
date: 2021-12-30T109:26:46+02:00
---
# Bibliothekarische Metadatenformate: Datenaustausch
Kurs im [MAS Bibliotheks- und Informationswissenschaft](https://www.biw.uzh.ch/de/masbiw.html), 5. Januar 2024 von Silvia Witzig

[Präsentation](https://witzigs.gitlab.io/mas-biw-metadata-slides/)

### Lernziele und Inhalte

Inhalte:
* Vorstellen der Schnittstellen OAI-PMH und SRU
* Ausblick zum Thema Linked Data und BIBFRAME
 
Lernziele:
* Die Studierenden kennen im Bibliotheksbereich verbreitete Schnittstellen.
* Sie machen erste Erfahrungen mit der praktischen Arbeit mit OAI-PMH.